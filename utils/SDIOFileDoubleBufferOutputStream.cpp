#include "config/stm32plus.h"
#include "config/filesystem.h"
#include "config/stream.h"

#include "SDIOFileDoubleBufferOutputStream.h"

namespace stm32plus {
  /**
   * Write a byte.
   * @param[in] c The byte to write.
   * @return false and E_BUFFER_FULL if the buffer has no space available for writing.
   */

  bool SDIOFileDoubleBufferOutputStream::write(uint8_t c) {

    if(_readPtr == _writePtr && _wrappedWrite)
      return false;

    *_writePtr++=c;

    if(_writePtr == _buffer + _bufferSize) {
      _writePtr=_buffer;
      _chunkFull=true;
      _wrappedWrite=true;
    } else if(_writePtr == _buffer + _bufferSize/2) {
      _chunkFull=true;
    }

    return true;
  }

  /**
   * Write many bytes.
   * @param[in] buffer The buffer of data to write.
   * @param[in] size The amount of bytes in the buffer.
   * @return false and E_BUFFER_FULL if the buffer has no space available for writing. In this case no data is written.
   */

  bool SDIOFileDoubleBufferOutputStream::write(const void *buffer,uint32_t size) {

    uint32_t available;
    const uint8_t *ptr;

    // get the amount available to write

    if(_readPtr == _writePtr)
      available=_wrappedWrite ? 0 : _bufferSize;
    else if(_readPtr > _writePtr)
      available=_readPtr - _writePtr;
    else
      available=_bufferSize - (_writePtr - _readPtr);

    if(size > available)
      return false;

    // do the write

    ptr=static_cast<const uint8_t *> (buffer);
    while(size--)
      write(*ptr++);

    return true;
  }

  bool SDIOFileDoubleBufferOutputStream::flush() {

    uint32_t available;

    if(_readPtr == _writePtr)
      available=_wrappedWrite?0:_bufferSize;
    else if(_readPtr > _writePtr)
      available=_bufferSize - (_readPtr - _writePtr);
    else
      available=_writePtr - _readPtr;

    while(available!=0) {
      uint32_t count=(available>_bufferSize/2)?(_bufferSize/2):available;
      void* ptr=const_cast<uint8_t *> (_readPtr);
      if(!_fileOutputStream->write(ptr, count))
        return false;
      _readPtr+=count;
      available-=count;
      if(_readPtr>=_buffer+_bufferSize) _readPtr=_buffer;
    }

    // reset to the origin
    _writePtr=_readPtr=_buffer;
    _chunkFull=false;
    _wrappedWrite=false;

    return true;
  }

  bool SDIOFileDoubleBufferOutputStream::commit() {

    if(!_chunkFull)
      return false;

    void* ptr=const_cast<uint8_t *> (_readPtr);
    if(!_fileOutputStream->write(ptr, _bufferSize/2))
      return false;

    if(_readPtr == _buffer) {
      _readPtr+=_bufferSize/2;
    } else if(_readPtr == _buffer+_bufferSize/2) {
      _readPtr=_buffer;
    } else {
      return false;
    }

    if(!(_readPtr == _writePtr && _wrappedWrite))
      _chunkFull=false;
    _wrappedWrite=false;

    return true;
  }
}
// vim:set ts=2 sw=2 et:
