#pragma once

#include "config/stm32plus.h"
#include "config/sdcard.h"
#include "config/filesystem.h"

class SDCard {
public:
	SDCard(bool autoInit = true);
	~SDCard();

	bool initialise();
	stm32plus::FileSystem* getFileSystem();
	stm32plus::File* getFile(const char *filename);
	stm32plus::File* createFile(const char *filename);
	bool deleteFile(const char *filename);
	stm32plus::OwnedFileOutputStream* getFileOutputStream(const char *filename);
	stm32plus::OwnedFileInputStream* getFileInputStream(const char *filename);
private:
	stm32plus::FileSystem *_fs;
	stm32plus::NullTimeProvider _timeProvider;
	stm32plus::SdioDmaSdCard *_sdcard;
};
