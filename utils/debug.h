#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/usart.h"
#include "config/stream.h"
#include "../machine/buzzer.h"
#include "../machine/irsensor.h"
#include "SDIOFileDoubleBufferOutputStream.h"
#include "debugstream.h"
#include "sdcard.h"

extern SDCard sdcard;
extern DebugStream<stm32plus::Usart1<> > debug;
extern Buzzer buzzer;
extern IRSensor ir;
extern stm32plus::GpioPinRef dleds[6];
extern stm32plus::SDIOFileDoubleBufferOutputStream sdiost;
extern stm32plus::TextOutputStream debugBufst;

extern const char* endl;

void tic();
uint32_t toc();

#define MAZEDEBUGx
#define PRINTTARGETx
#define SENSORDEBUGx
#define DIAGONALDEBUGx
#define CIRCUITx
