#include "../libmazesolver/maze.h"
#include "../libmazesolver/wallsource.h"
#include "../machine/machine.h"

namespace MazeSolver {
	class MachineWallSource : public WallSource {
		public:
			MachineWallSource(Machine *_machine):WallSource(),machine(_machine){}
			~MachineWallSource(){}
			bool isSetWall(Coord c, Direction d, Direction rd){
				// TODO: implement this
				return machine->isSetWall(rd.half);
			}
		private:
			Machine *machine;
	};
}
