#pragma once

#include "config/stm32plus.h"
#include "config/usart.h"

template<class T> 
class DebugStream : public stm32plus::TextOutputStream{
public:
	DebugStream(uint32_t baud):TextOutputStream(usartos),usart(baud),usartos(usart){
	}
private:
	T usart;
	stm32plus::UsartPollingOutputStream usartos;
};
