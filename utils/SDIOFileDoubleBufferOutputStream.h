/*
 * This file is a part of the open source stm32plus library.
 * Copyright (c) 2011,2012,2013,2014,2015 Andy Brown <www.andybrown.me.uk>
 * Please see website for licensing terms.
 */

#pragma once

#include "config/stm32plus.h"
#include "config/filesystem.h"

namespace stm32plus {

  /**
   * @brief 
   *
   * This class 
   */

  class SDIOFileDoubleBufferOutputStream : public OutputStream {
    protected:
      FileOutputStream *_fileOutputStream;
      uint8_t *_buffer;
      uint32_t _bufferSize;
      bool _chunkFull;
      bool _wrappedWrite;
      volatile uint8_t *_readPtr;
      volatile uint8_t *_writePtr;

    public:
      SDIOFileDoubleBufferOutputStream(FileOutputStream* os): _fileOutputStream(os),_bufferSize(2048),_chunkFull(false),_wrappedWrite(false) {
        // Double buffering
        _readPtr=_writePtr=_buffer=static_cast<uint8_t *> (malloc(_bufferSize));
      }
      virtual ~SDIOFileDoubleBufferOutputStream() {
        free(_buffer);
      }

      bool isChunkFull() const { return _chunkFull; }

      bool commit(uint32_t size,uint32_t& actuallyWritten);
      bool commit();

      void setFile(FileOutputStream* os) { _fileOutputStream=os; }

      virtual bool write(uint8_t c) override;
      virtual bool write(const void *buffer,uint32_t size) override;

      virtual bool flush() override;

      // common methods

      virtual bool close() override {
        return true;
      }
  };
}
// vim:set ts=2 sw=2 et:
