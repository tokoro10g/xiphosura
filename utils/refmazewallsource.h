#pragma once
#include "../libmazesolver/maze.h"
#include "../libmazesolver/wallsource.h"

namespace MazeSolver {
	class RefMazeWallSource : public WallSource {
		public:
			RefMazeWallSource(Maze *_refMaze):WallSource(),refMaze(_refMaze){}
			~RefMazeWallSource(){}
			bool isSetWall(Coord c, Direction d, Direction rd){
				return refMaze->isSetWall(c,d);
			}
		private:
			Maze *refMaze;
	};
}
