#pragma once

#include "mymath.h"

class Filter {
	public:
		Filter(const float q0, float _dt):q(q0),dt(_dt){}
		float getValue() const{ return q; }
	protected:
		float q;
		float dt;
};

class BWFilter : public Filter {
	public:
		BWFilter(const float q0, float _dt, float _gain, float _coeff):Filter(q0,_dt),gain(_gain),coeff(_coeff),xv(0.f){}
		void update(const float input){
			q=(input/gain-xv)+(coeff*q);
			xv=input/gain;
		}
	protected:
		float gain;
		float coeff;
		float xv;
};

class CompFilter : public Filter {
	public:
		CompFilter(const float q0, float _gain):Filter(q0,0),gain(_gain){}
		void update(const float input){
			q=gain*q+input*(1-q);
		}
	protected:
		float gain;
};
