#include "debug.h"

using namespace stm32plus;

SDCard sdcard(false);
DebugStream<Usart1<> > debug(921600);
SDIOFileDoubleBufferOutputStream sdiost(nullptr);
TextOutputStream debugBufst(sdiost);

Buzzer buzzer;
IRSensor ir;
GpioB<DefaultDigitalOutputFeature<13,14,15>> dled123;
GpioC<DefaultDigitalOutputFeature<6,7>> dled45;
GpioA<DefaultDigitalOutputFeature<8>> dled6;
GpioPinRef dleds[6] = {
	dled123[15],
	dled123[14],
	dled123[13],
	dled45[6],
	dled45[7],
	dled6[8]
};

const char* endl="\n";

void tic() {
	MillisecondTimer::reset();
}

uint32_t toc() {
	return MillisecondTimer::millis();
}
