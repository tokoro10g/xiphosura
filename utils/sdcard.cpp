#include "sdcard.h"

using namespace stm32plus;

SDCard::SDCard(bool autoInit):_timeProvider() {
	if(autoInit) { initialise(); }
}

SDCard::~SDCard() {
	delete _fs;
	delete _sdcard;
}

bool SDCard::initialise() {
	_sdcard = new SdioDmaSdCard();
	if(errorProvider.hasError()) return false;
	if(!FileSystem::getInstance(*_sdcard,_timeProvider,_fs)) return false;
	return true;
}

FileSystem* SDCard::getFileSystem() {
	return _fs;
}

File* SDCard::createFile(const char *filename) {
	File *file;
	if(!_fs->createFile(filename)) return nullptr;
	if(!_fs->openFile(filename,file)) return nullptr;
	return file;
}

bool SDCard::deleteFile(const char *filename) {
	return _fs->deleteFile(filename);
}

File* SDCard::getFile(const char *filename) {
	FileInformation *info;
	File *file;
	if(!_fs->getFileInformation(filename,info)) return nullptr;
	if((info->getAttributes() & FileInformation::ATTR_DIRECTORY)!=0) {
		delete info;
		return nullptr;
	}
	delete info;
	if(!_fs->openFile(filename,file)) return nullptr;
	return file;
}

OwnedFileOutputStream* SDCard::getFileOutputStream(const char *filename) {
	File *file = getFile(filename);
	if(file == nullptr) return nullptr;
	return new OwnedFileOutputStream(file);
}
OwnedFileInputStream* SDCard::getFileInputStream(const char *filename) {
	File *file = getFile(filename);
	if(file == nullptr) return nullptr;
	return new OwnedFileInputStream(file);
}
