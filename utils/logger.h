#pragma once

#include "config/nvic.h"
#include "config/string.h"
#include "config/sdcard.h"
#include "config/filesystem.h"

class Logger {
public:
	Logger();

private:
	FileSystem *_fs;
	NullTimeProvider _timeProvider;
	SdioDmaSdCard *_sdcard;
};
