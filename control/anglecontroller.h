#include "../machine/motor.h"
#include "../machine/encoder.h"
#include "../utils/mymath.h"
#include "../utils/mymath_machine.h"
#include "pidcontroller.h"

using namespace stm32plus;

template<typename T>
class AngleController {
public:
	AngleController(T Kp,T Ki,T Kd,bool reseti=true,T isat=0):pid(Kp,Ki,Kd,reseti,isat),u(T(0)),rp(T(0)){}
	~AngleController(){}
	void setGain(T Kp,T Ki,T Kd,T isat=0){
		pid.setGain(Kp,Ki,Kd,isat);
	}
	inline void reset(){
		pid.reset();
		u=0; rp=0;
	}
	void update(T r, T x){
		float dr = r-rp;
		MyMath::normalize(dr, MyMath::Machine::PIInGyroValue);

		float e = r-x;
		MyMath::normalize(e, MyMath::Machine::PIInGyroValue);

		pid.update(e,0);
		u = pid.getOutput() + 0.0681f*dr;
		rp = r;
	}
	T getOutput() const {
		return u;
	}
private:
	PIDController<T> pid;
	T u;
	T rp;
};
