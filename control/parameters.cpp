#include "parameters.h"

// v_start, v_end, v_max, a_max
Trajectory::Parameters p_straight_start(0,450,450,2000);
Trajectory::Parameters p_straight(450,450,450,2000);
Trajectory::Parameters p_straight_end(450,0,450,2000);
Trajectory::Parameters p_turn(450,450,450,2000);

Trajectory::Parameters p_miniturn(0,0,10,40);
Trajectory::Parameters p_ministraight(0,0,100,5000);

Trajectory::Parameters p_faststraight_start(0,950,3000,12000);
Trajectory::Parameters p_faststraight(950,950,3000,12000);
Trajectory::Parameters p_faststraight_to_endturn(950,850,3000,12000);
Trajectory::Parameters p_faststraight_end(950,0,3000,12000);

Trajectory::Parameters p_faststraight_max_start(0,3000,3000,12000);
Trajectory::Parameters p_faststraight_max(3000,3000,3000,12000);
Trajectory::Parameters p_faststraight_max_from_turn(950,3000,3000,12000);
Trajectory::Parameters p_faststraight_max_to_turn(3000,950,3000,12000);
Trajectory::Parameters p_faststraight_max_end(3000,0,3000,12000);

Trajectory::Parameters p_fastturn(950,950,950,5000);
Trajectory::Parameters p_fastendturn(850,850,950,5000);
Trajectory::Parameters p_faststraight_startend(0,0,3000,12000);
