#include "../machine/motor.h"
#include "../machine/encoder.h"
#include "pidcontroller.h"

using namespace stm32plus;

template<typename T, class TMotor, class TEncoder>
class MotorController : public TMotor, public TEncoder {
public:
	MotorController(T Kp,T Ki,T Kd,T Kff,bool reseti=true,T isat=0):TMotor(),TEncoder(),pid(Kp,Ki,Kd,reseti,isat),_Kff(Kff){}
	~MotorController(){}
	void setGain(T Kp,T Ki,T Kd,T isat=0){
		pid.setGain(Kp,Ki,Kd,isat);
	}
	inline void reset(){
		pid.reset();
		TEncoder::reset();
	}
	void update(T r){
		static uint8_t overCnt=0;
		// NOTE: should be called after captureSpeed
		int16_t x=TEncoder::getSpeed();
		pid.update(r,x);
		if(pid.getIntegrator()>2000){
			overCnt++;
		} else {
			overCnt=0;
		}
		//if(overCnt>70){ throw -1; }
		TMotor::setOutput(-pid.getOutput()-_Kff*r);
	}
private:
	PIDController<T> pid;
	T _Kff;
};
