#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/spi.h"
#include "config/timing.h"
#include "config/nvic.h"
#include "config/string.h"
#include "config/sdcard.h"
#include "config/filesystem.h"

#include "config/stdperiph.h"
#include "fwlib/f4/stdperiph/inc/stm32f4xx_dbgmcu.h"

#include "machine/motor.h"
#include "machine/button.h"
#include "machine/buzzer.h"
#include "machine/encoder.h"
#include "machine/machine.h"

#include "control/parameters.h"

#include "devices/mpu6500.h"

#include "libmazesolver/maze.h"
#include "libmazesolver/graph.h"
#include "libmazesolver/agent.h"

#include "utils/debug.h"
#include "utils/sdcard.h"
#include "utils/mazeloader.h"
#include "utils/machinewallsource.h"
#include "utils/refmazewallsource.h"

using namespace stm32plus;
using namespace MazeSolver;

class Core {
	private:
		bool logEnabled;
		bool logWritten;
		GpioC<DefaultDigitalInputFeature<13>> gpioc;
		EdgeButton button;
		Machine machine;

		Maze maze;
		Maze backupMaze;
#ifdef MAZEDEBUG
		Maze refMaze;
#endif
		Graph graph;
		MachineWallSource wallSource;
#ifdef MAZEDEBUG
		RefMazeWallSource refWallSource;
#endif
		Agent agent;

		uint16_t index;
		Direction dir;
		uint16_t goal;
		volatile float angle;

	public:
		Core():
			logEnabled(true),
			logWritten(false),
			button(gpioc[13]),
			machine(),
			wallSource(&machine),
#ifdef MAZEDEBUG
			refWallSource(&refMaze),
			agent(&maze,&refWallSource,&graph),
#else
			agent(&maze,&wallSource,&graph),
#endif
			index(15),
			goal(15),
			angle(0) {}
		void initialise() {
			MillisecondTimer::initialise();

#ifdef MAZEDEBUG
			//MazeLoader::loadEmpty(16, 16, 7, 7, maze);
			//MazeLoader::loadTestMaze(16, 16, 7, 7, refMaze);
			MazeLoader::loadEmpty(16, 16, 1, 0, maze);
			MazeLoader::loadTestMaze(16, 16, 1, 0, refMaze);
#else
			//MazeLoader::loadEmpty(16, 16, 7, 7, maze); // full-size competiton
			//MazeLoader::loadEmpty(16, 16, 1, 0, maze); // full-size debug
			MazeLoader::loadEmpty(16, 16, 1, 0, maze); // full-size debug
			//MazeLoader::loadEmpty(16, 16, 3, 3, maze); // full-size debug
#endif
			backupMaze=maze;

			graph.loadEmpty(16,16);
			graph.loadMaze(&maze);
			graph.resetCost();

			DBGMCU_Config(DBGMCU_STOP,ENABLE);
			DBGMCU_APB1PeriphConfig(DBGMCU_TIM2_STOP, ENABLE);
			DBGMCU_APB2PeriphConfig(DBGMCU_TIM11_STOP, ENABLE);

			if(!sdcard.initialise()) {
				buzzer.playErrorSound();
				logEnabled = false;
			}

			MillisecondTimer::delay(200);
			buzzer.playPowerOnSound();
			machine.initialise();
			machine.deactivate();

			const char* filename = "test.mat";

			OwnedFileOutputStream *sdos;
			if(logEnabled) {
				machine.enableLog();
				sdcard.deleteFile(filename);
				File *file = sdcard.createFile(filename);
				sdos = new OwnedFileOutputStream(file);
				sdiost.setFile(sdos);
			}

			machine.unblock();
			MillisecondTimer::delay(40);
			if(machine.getBattery() < 3100 * 2) {
				dleds[2].set(); dleds[3].set();
				buzzer.playErrorSound();
			}
			machine.block();
		}

		int8_t waitIR(){
			machine.unblock();
			while(machine.getIrRF()<7000){
				button.updateState();
				if(button.getState()==EdgeButton::RisingEdge){
					buzzer.playConfirmSound(); return -1;
				}
				MillisecondTimer::delay(20);
			}
			machine.block();
			buzzer.playConfirmSound();
			for(auto led : dleds) { led.reset(); }
			return 0;
		}

		uint8_t select(uint8_t max){
			int8_t val=0;
			bool changed=false;
			machine.setEncoderCounter(0);
			while(1){
				int16_t enc=machine.getEncoderCounter();
				if(enc>1500){
					val++; machine.setEncoderCounter(0); changed=true;
				} else if(enc<-1500){
					val--; machine.setEncoderCounter(0); changed=true;
				}

				if(val<0) val=max;
				else if(val>max) val=0;

				if(changed){
					for(uint8_t i=0;i<6;i++){
						dleds[i].setState(val&(1<<i));
					}
				}

				button.updateState();
				if(button.getState()==EdgeButton::RisingEdge){
					for(auto led : dleds) { led.reset(); }
					//buzzer.playConfirmSound();
					return val;
				}

				MillisecondTimer::delay(20); changed=false;
			}
		}

		void wait(){
			while(1){
				button.updateState();
				if(button.getState()==EdgeButton::RisingEdge){
					buzzer.playConfirmSound(); menu();
				}
				MillisecondTimer::delay(20);
			}
		}

		void menu(){
			debug<<"MENU"<<endl;
			uint8_t mode=select(3);
			switch(mode){
				case 0:  return;
				case 1:  return runMode();
				case 2:  return mazeMode();
				case 3:  return sensorMode();
				default: break;
			}
			return;
		}

		void runMode(){
			debug<<"\trunMode"<<endl;
			uint8_t mode=select(4); int8_t result=0;
			switch(mode){
				case 0:  return;
				case 1:  result=searchRunMode(); break;
				case 2:  result=fastRunMode(false, false); break;
				case 3:  result=fastRunMode(false, true); break;
				case 4:  result=fastRunMode(true, true); break;
				default: break;
			}
			switch(result){
				case -1: debug<<"ERROR"<<endl; break;
				default: break;
			}
			return;
		}

		void initialiseRun(){
			agent.setIndex(15);
			agent.setDir(Maze::DirNorth);
			agent.reroute();
			index=15;
			angle=0;
			dir=Maze::DirNorth;

			machine.deactivate();
			machine.block();
			machine.calibrateGyro();
			machine.unblock();

			machine.setState(0,0,0);
			machine.resetTargetSequence();
			machine.resetControllers();
			//machine.setWallAdjust();
			machine.setNeutralSideSensorValue();
			machine.activate();
		}

		int8_t procSearch(uint16_t _goal, bool further=false){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;
			goal=_goal;

			while(1){
				if(machine.isTargetSequenceEmpty()){
					int16_t nextIndex;
					//machine.setWallAdjust();
					//machine.selectLowSpeedGain();
					flushLog();
					dleds[5].set();
					nextIndex=agent.stepMaze(goal, true);
					dleds[5].reset();
					flushLog();
					//debug<<nextIndex<<endl;
					if(nextIndex<0){
						if(!further){
							buzzer.playErrorSound();
							machine.deactivate();
							maze=backupMaze;
							graph.reset();
							agent.reroute();
						}
						return -2;
					}
					flushLog();
					if(nextIndex==index||agent.isPullBack(index,nextIndex,dir,agent.getDir())){
						moveBack();
						flushLog();
						continue;
					}
					dir=agent.getDir();
					Coord c=agent.getCoord();
					Position p(c.x*180,c.y*180,0);
					p.angle=-(float)(dir.half==0x2)*PI/2.f-(float)(dir.half==0x4)*PI+(float)(dir.half==0x8)*PI/2.f;
					p.x+=(dir.half==0x2)*90-(dir.half==0x8)*90;
					p.y+=(dir.half==0x1)*90-(dir.half==0x4)*90;
#ifndef MAZEDEBUG
					if(fabs(normalized(angle-p.angle,PI))<0.2f){
						machine.pushTarget(p, new MotionLinear(new EasingPoly5()), p_straight);
					} else {
						machine.pushTarget(p, new MotionSmoothArc(new EasingLinear()), p_turn);
					}
#endif
					angle=normalized(p.angle,PI);
					index=nextIndex;
					if(nextIndex==goal){
						backupMaze=maze;
						return 0;
					}
					flushLog();
				}

				flushLog();
				if(!machine.isActive()) break;

				MillisecondTimer::delay(1);
			}
			flushLog();

			buzzer.playErrorSound();
			return -1;
		}

		void flushLog() {
			if(logEnabled){
				if(sdiost.isChunkFull()){
					dleds[4].set();
					sdiost.commit();
					dleds[4].reset();
				}

				if(!logWritten && !machine.isActive()){
					dleds[4].set();
					sdiost.flush();
					logWritten = true;
					dleds[4].reset();
				}
			}
		}

		void moveBack(){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;

#ifndef MAZEDEBUG
			if(machine.getLastVelocity()!=0){
				machine.pushTargetDiff(Position(-90.f*sin(angle),90.f*cos(angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_end);
				MillisecondTimer::delay(500);
				machine.disableLog();
				flushLog();
			}
			//machine.resetWallAdjust();
			//machine.setFrontWallAdjust();
			MillisecondTimer::delay(400);
			flushLog();

			machine.pushTargetDiff(Position(0,0,PI/2.f), new MotionTurn(new EasingPoly5()), p_miniturn);
			MillisecondTimer::delay(400);
			flushLog();
			machine.pushTargetDiff(Position(0,0,PI/2.f), new MotionTurn(new EasingPoly5()), p_miniturn);
			MillisecondTimer::delay(400);
			flushLog();
			machine.pushTargetDiff(Position(-90.f*sin(angle+PI),90.f*cos(angle+PI),0.f), new MotionLinear(new EasingPoly5()), p_straight_start);
#endif
			angle=normalized(angle+PI,PI);
			buzzer.setFreqency(1576);
			buzzer.on();
			MillisecondTimer::delay(10);
			buzzer.off();
			//machine.setWallAdjust();
			dir=agent.getDir();
			machine.enableLog();
			return;
		}

		int8_t searchRunMode(){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;

			debug<<"\t\tsearchRunMode"<<endl;
			uint8_t param=select(4);
			if(param==0) return 0;

//			p_straight_start=Trajectory::Parameters(0,v,v,2000);
//			p_straight=Trajectory::Parameters(v,v,v,2000);
//			p_straight_end=Trajectory::Parameters(v,0,v,2000);
//			p_turn=Trajectory::Parameters(v,v,v,2000);
//			p_miniturn=Trajectory::Parameters(0,0,10,40);
//			p_ministraight=Trajectory::Parameters(0,0,100,40);
//
			if(waitIR()<0) return 0;
			MillisecondTimer::delay(500);

			initialiseRun();

			machine.pushTarget(Position(0,90,0), new MotionLinear(new EasingPoly5()), p_straight_start);

			if(procSearch(maze.getGoalNodeIndex())<0) return -1;

			agent.reroute();
			// FIXME: further search
			//if(procSearch(15)<0) return -1;

			while(1){
				int16_t tempGoal=agent.searchCandidateNode(15);
				if(index==15 && tempGoal<0){ break; }
				if(tempGoal<0){ tempGoal=15; }
				agent.reroute();
				int8_t result=procSearch(tempGoal,true);
				if(result>=0){
					graph.getNodePointer(tempGoal)->setVisited();
				}
				if(result==-1) return -1;
			}


			//machine.resetWallAdjust();
			machine.pushTargetDiff(Position(-90.f*sin(angle),90.f*cos(angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_end);
			MillisecondTimer::delay(1500);
			buzzer.playConfirmSound();
			machine.deactivate();
			return 0;
		}

		bool pushDiagonalTrajectory(int16_t alphaFrom, int16_t alphaTo) {
			using namespace Trajectory;
			int16_t mod = (alphaTo - alphaFrom) % 360;
			if((alphaTo - alphaFrom) % 360 == 0) return false;

			bool ret = true;

			float alphaFrom_rad = (float)alphaFrom / 180.f * MyMath::PI;
			float alphaTo_rad = (float)alphaTo / 180.f * MyMath::PI;

			switch(abs(mod)) {
				case 180:
					{
						// 180 deg turn
						Position po = MotionSmoothArc::calcDest(machine.getLastPosition(), 180.f, alphaTo_rad);
						machine.pushTarget(po, new MotionSmoothArc(new EasingLinear()), p_fastturn);
						debug << "SmoothArc," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
					}
					break;
				case 45:
				case 315:
					{
						// 45 deg turn
						if(alphaFrom % 90 != 0) {
							machine.pushTargetDiff(71.9257f*Position(-sin(alphaFrom_rad), cos(alphaFrom_rad), 0.f), new MotionLinear(new EasingLinear()), p_fastturn);
						}
						Position po = MotionSmoothArc::calcDest(machine.getLastPosition(), 101.9534f, alphaTo_rad);
						machine.pushTarget(po, new MotionSmoothArc(new EasingLinear()), p_fastturn);
						debug << "SmoothArc," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
						if(alphaFrom % 90 == 0) {
							machine.pushTargetDiff(71.9257f*Position(-sin(alphaTo_rad), cos(alphaTo_rad), 0.f), new MotionLinear(new EasingLinear()), p_fastturn);
						}
					}
					break;
				case 90:
				case 270:
					{
						// 90 deg turn
						float dist;
						if(alphaFrom % 90 == 0) {
							dist = 145.f * sqrt(2.f);
						} else {
							dist = 180.f;
						}
						Position po = MotionSmoothArc::calcDest(machine.getLastPosition(), dist, alphaTo_rad);
						machine.pushTarget(po, new MotionSmoothArc(new EasingLinear()), p_fastturn);
						debug << "SmoothArc," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
					}
					break;
				case 135:
				case 225:
					{
						// 135 deg turn
						if(alphaFrom % 90 != 0) {
							machine.pushTargetDiff(18.6728f*Position(-sin(alphaFrom_rad), cos(alphaFrom_rad), 0.f), new MotionLinear(new EasingLinear()), p_fastturn);
						}
						Position po = MotionSmoothArc::calcDest(machine.getLastPosition(), 180.2f, alphaTo_rad);
						machine.pushTarget(po, new MotionSmoothArc(new EasingLinear()), p_fastturn);
						debug << "SmoothArc," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
						if(alphaFrom % 90 == 0) {
							machine.pushTargetDiff(18.6728f*Position(-sin(alphaTo_rad), cos(alphaTo_rad), 0.f), new MotionLinear(new EasingLinear()), p_fastturn);
						}
					}
					break;
				default:
					ret = false;
					break;
			}
			return ret;
		}

		int8_t fastRunMode(bool useDiagonal, bool wallAdjust){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;

			machine.setWallCorrection(wallAdjust);

			debug<<"\t\tfastRunMode"<<endl;
			uint8_t param=select(6);
			if(param==0) return 0;

			uint16_t rv=700+param*50;
			uint16_t maxv=1200+param*400;
			uint16_t ra=3000+param*1450;
			//const Trajectory::Parameters p_faststraight_start(0,rv,2000,4000);
			//const Trajectory::Parameters p_faststraight(rv,rv,2000,4000);
			//const Trajectory::Parameters p_faststraight_end(rv,0,rv,2000);
			//const Trajectory::Parameters p_fastturn(rv,rv,1200,2000);
			p_faststraight_start = Trajectory::Parameters(0,rv,maxv,ra);
			p_faststraight = Trajectory::Parameters(rv,rv,maxv,ra);
			p_faststraight_end = Trajectory::Parameters(rv,0,maxv,ra);
			p_fastturn = Trajectory::Parameters(rv,rv,rv*2,ra);

			if(waitIR()<0) return 0;
			MillisecondTimer::delay(500);

			initialiseRun();

			goal=maze.getGoalNodeIndex();

			//machine.selectHighSpeedGain();

			Position pTo(0,90,0);
			Position pPivot(pTo),pFrom(pTo);
			int16_t nextIndex;
			uint8_t straightCount=0;
			bool firstFlag=true;

			int16_t alphaStart = 0;
			int16_t alphaHalfway = 0;
			int16_t alphaHalfway_pre = 0;
			int16_t alphaEnd = 0;
			bool contSequence = false;
			bool contSequence_pre = false;

			Direction dirdiff_pre; dirdiff_pre.half = 0;

			while(index!=goal){
				nextIndex=agent.stepMaze(goal, false);
				if(nextIndex<0){
					buzzer.playErrorSound();
					machine.deactivate();
					return -1;
				}
				pFrom=pPivot;
				pPivot=pTo;

				Coord c=graph.getNodePointer(nextIndex)->getCoord(maze.getWidth()); // Coordinate with direction north and east
				pTo=Position(c.x*180,c.y*180,0);
				pTo.x+=(c.dir.half==0x2)*90;
				pTo.y+=(c.dir.half==0x1)*90;
				pTo.angle=-(float)(agent.getDir().half==0x2)*PI/2.f-(float)(agent.getDir().half==0x4)*PI+(float)(agent.getDir().half==0x8)*PI/2.f;

				if(useDiagonal){
					// trajectory with diagonal path {{{
					Direction dirdiff = dir-agent.getDir();
					if(!dirdiff.bits.NORTH) {
						Position po = pPivot - 55.f * Position(-sin(pPivot.angle), cos(pPivot.angle), 0.f);
						if(firstFlag){
							machine.pushTarget(Position(0,35,0), new MotionLinear(new EasingPoly5()), p_faststraight_start);
							debug << "Linear," << (int32_t)0 << "," << (int32_t)35 << "," << (int32_t)0 <<endl;
							if(straightCount>0){
								machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_faststraight);
								debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
							}
							firstFlag=false;
						} else if(straightCount>0){
							machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_faststraight);
							debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
						}
					}

					int16_t dalpha = (dirdiff.bits.WEST) * 90 - (dirdiff.bits.EAST) * 90;

					if((dirdiff_pre.bits.WEST && dirdiff.bits.EAST) || (dirdiff_pre.bits.EAST && dirdiff.bits.WEST)) {
						if(alphaHalfway % 90 == 0) {
							alphaHalfway += dalpha / 2;
						}
						contSequence = true;
					} else {
						alphaHalfway += dalpha;
						contSequence = false;
					}
					alphaEnd += dalpha;

					// normalize start angle
					if(alphaStart > 180) alphaStart -= 360;
					else if(alphaStart <= -180) alphaStart += 360;
					// normalize angles (here, -180 deg is included exceptionally)
					if(alphaHalfway > 180) alphaHalfway -= 360;
					else if(alphaHalfway < -180) alphaHalfway += 360;
					if(alphaEnd > 180) alphaEnd -= 360;
					else if(alphaEnd < -180) alphaEnd += 360;

					debug << (int16_t)dirdiff.half << "," << alphaStart << "," << alphaHalfway << "," << alphaEnd << "," << (int16_t)contSequence << "," << (int16_t)contSequence_pre << endl;

					// the beginning point here is 55 mm behind pPivot or the end of previous turn
					if(dirdiff.bits.NORTH) {
						if(!dirdiff_pre.bits.NORTH) {
							if(contSequence_pre) {
								// make the end of an existing turn
								// next angle : alphaHalfway_pre
								if(pushDiagonalTrajectory(alphaStart, alphaHalfway_pre)) {
									float alphaHalfway_pre_rad = (float)alphaHalfway_pre/180.f * PI;
									Position po(pFrom.x, pFrom.y, alphaHalfway_pre_rad);
									machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_fastturn);
									debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;

									alphaStart = alphaHalfway_pre;
								}
							}
							// then, terminate the whole turn sequence
							// next angle : alphaEnd
							pushDiagonalTrajectory(alphaStart, alphaEnd);
							alphaStart = alphaEnd;
							alphaHalfway = alphaEnd;
						}
						// continue counting
						straightCount++;
					} else if(contSequence_pre && !contSequence) {
						// previous turn has been terminated
						// next angle : alphaHalfway_pre
						pushDiagonalTrajectory(alphaStart, alphaHalfway_pre);

						float alphaHalfway_pre_rad = (float)alphaHalfway_pre/180.f * PI;
						Position po(pFrom.x, pFrom.y, alphaHalfway_pre_rad);
						machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_fastturn);
						debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;

						straightCount=0;
						alphaStart = alphaHalfway_pre;
					} else {
						// do nothing
						straightCount=0;
					}

					// end
					if(nextIndex==goal){
						if(contSequence) {
							// make the end of an existing turn
							// next angle : alphaHalfway
							if(pushDiagonalTrajectory(alphaStart, alphaHalfway)) {
								float alphaHalfway_rad= (float)alphaHalfway/180.f * PI;
								Position po(pPivot.x, pPivot.y, alphaHalfway_rad);
								machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_fastturn);
								debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;

								alphaStart = alphaHalfway;
							}
						}
						if(alphaStart != alphaEnd) {
							// terminate the whole turn sequence
							// next angle : alphaEnd
							pushDiagonalTrajectory(alphaStart, alphaEnd);
						}
						if(straightCount > 0) {
							machine.pushTarget(pTo, new MotionLinear(new EasingPoly5()), p_faststraight);
						}
						Position po = pTo + 90.f * Position(-sin(pTo.angle),cos(pTo.angle),0.f);
						machine.pushTarget(po, new MotionLinear(new EasingPoly5()), p_faststraight_end);
						debug << "Linear," << (int32_t)(po.x*10) << "," << (int32_t)(po.y*10) << "," << (int32_t)((po.angle*10)*180.f/MyMath::PI) <<endl;
					}
					
					alphaHalfway_pre = alphaHalfway;
					if(alphaHalfway_pre > 180) alphaHalfway_pre -= 360;
					if(alphaHalfway_pre <= -180) alphaHalfway_pre += 360;
					contSequence_pre = contSequence;
					dirdiff_pre = dirdiff;
					// }}}
				} else {
					// trajectory without diagonal path {{{
					if(dir.half==agent.getDir().half){
						// straight
						straightCount++;
					} else {
						// turn after straight
						if(firstFlag){
							machine.pushTarget(Position(0,90,0), new MotionLinear(new EasingPoly5()), p_faststraight_start);
							if(straightCount>0){
								machine.pushTarget(pPivot, new MotionLinear(new EasingPoly5()), p_faststraight);
							}
							firstFlag=false;
						} else if(straightCount>0){
							machine.pushTarget(pPivot, new MotionLinear(new EasingPoly5()), p_faststraight);
						}
						machine.pushTarget(pTo, new MotionSmoothArc(new EasingLinear()), p_fastturn);
						straightCount=0;
					}

					// goal
					if(nextIndex==goal){
						if(straightCount>0){
							machine.pushTarget(pTo, new MotionLinear(new EasingPoly5()), p_faststraight);
						}
						machine.pushTargetDiff(Position(-90.f*sin(pTo.angle),90.f*cos(pTo.angle),0.f), new MotionLinear(new EasingPoly5()), p_faststraight_end);
					}
					// }}}
				}

				// prepare for the next step
				dir.half=agent.getDir().half;
				angle=normalized(pTo.angle,PI);
				index=nextIndex;
			}
			//machine.setWallAdjust();

			while(1){
				if(!machine.isActive()){
					buzzer.playErrorSound();
					flushLog();
					return -1;
				}
				if(machine.isTargetSequenceEmpty()){
					flushLog();
					break;
				}
				flushLog();
				MillisecondTimer::delay(10);
			}
			buzzer.playConfirmSound();
			//machine.selectLowSpeedGain();
			//machine.resetWallAdjust();
			machine.disableLog();
			MillisecondTimer::delay(1000);
			machine.enableLog();
			flushLog();
			//machine.setWallAdjust();

			agent.reroute();
			if(procSearch(15)<0) return -1;

			//machine.resetWallAdjust();
			machine.pushTargetDiff(Position(-90.f*sin(angle),90.f*cos(angle),0.f), new MotionLinear(new EasingPoly5()), p_straight_end);
			MillisecondTimer::delay(1500);
			buzzer.playConfirmSound();
			machine.deactivate();
			flushLog();
			return 0;
		}

		void mazeMode(){
			debug<<"\tmazeMode"<<endl;
			uint8_t param=select(2);
			if(param==0) return;
			return;
		}

		void sensorMode(){
			debug<<"\tsensorMode"<<endl;
			uint8_t param=select(8);
			if(param==0) return;
			machine.setThresholds(1300+100*param,2000,3600,5800+200*param);
			return;
		}

		void run() {
			while(1){
				wait();
			}
			while(1);
		}
};

int main() {
	Core core;
	core.initialise();
	core.run();
	return 0;
}
