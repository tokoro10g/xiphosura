#pragma once

template<class TSPI>
class MPU6500 : public TSPI{
public:
	MPU6500(typename TSPI::Parameters& params):TSPI(params){
		this->setNss(true);
		this->enablePeripheral();
	}
	bool test(){
		unsigned char rx;
		if(!this->readReg(0x75, rx)) return false;
		return (rx==0x70);
	}
	void setup(){
		this->writeReg(0x19, 0x07);
		this->writeReg(0x1b, 0x18);
		this->writeReg(0x6b, 0x00);
	}
	bool readReg(uint8_t reg, uint8_t& byte) {
		uint8_t data[2];
		uint8_t recv[2];
		data[0] = reg | 0x80;
		data[1] = 0x00;
		this->setNss(false);
		if(!this->send(data, 2, recv)) {
			this->setNss(true);
			return false;
		}
		this->waitForIdle();
		this->setNss(true);
		byte = recv[1];
		return true;
	}
	bool writeReg(uint8_t reg, uint8_t val) {
		this->setNss(false);
		if(!this->send(&reg, 1)) {
			this->setNss(true);
			return false;
		}
		if(!this->send(&val, 1)) {
			this->setNss(true);
			return false;
		}
		this->waitForIdle();
		this->setNss(true);
		return true;
	}
	inline int16_t readInt16(uint8_t addr){
		union{
			uint16_t u;
			int16_t i;
		} _u2i;
		addr |= 0x80;
		unsigned char rx[2];
		this->setNss(false);
		this->send(&addr, 1);
		this->receive(rx,2);
		this->waitForIdle();
		this->setNss(true);
		_u2i.u=(rx[0]<<8)|rx[1];
		return _u2i.i;
	}
	inline int16_t readAccX(){
		return readInt16(0x3b);
	}
	inline int16_t readAccY(){
		return readInt16(0x3d);
	}
	inline int16_t readAccZ(){
		return readInt16(0x3f);
	}
	inline int16_t readTemp(){
		return readInt16(0x41);
	}
	inline int16_t readGyrX(){
		return readInt16(0x43);
	}
	inline int16_t readGyrY(){
		return readInt16(0x45);
	}
	inline int16_t readGyrZ(){
		return readInt16(0x47);
	}
private:
};
