#pragma once

#include "../control/controlTimer.h"
#include "../control/motorcontroller.h"
#include "../control/anglecontroller.h"

#include "../devices/mpu6500.h"

#include "../utils/debug.h"
#include "../utils/mymath.h"
#include "../utils/mymath_machine.h"

#include "../libtrajectory/targetsequence.h"
#include "../libtrajectory/target.h"
#include "../libtrajectory/position.h"
#include "../libtrajectory/functors.h"

#include <cmath>
#include <queue>

using namespace stm32plus;

class Machine {
	private:
		uint16_t SIDE_NEUTRAL_LEFT;
		uint16_t SIDE_NEUTRAL_RIGHT;

		uint16_t FRONT_THRESHOLD;
		uint16_t SIDE_THRESHOLD;
		uint16_t FRONT_ADJUST_THRESHOLD;
		uint16_t FRONT_NEUTRAL;

		uint16_t irLB, irLF, irRF, irRB, battery;

		uint32_t timestamp;

		MotorController<
			float,
			Motor< TimerChannel1Feature<>, TimerChannel2Feature<> >,
			Encoder<TimerEncL>
			> motorCtrlL;
		MotorController<
			float,
			Motor< TimerChannel3Feature<>, TimerChannel4Feature<> >,
			Encoder<TimerEncR>
			> motorCtrlR;

		AngleController<float> angleCtrl;

		ControlTimer<
			Timer5< Timer5InternalClockFeature, Timer5InterruptFeature >,
			Machine
			> motorCtrlTimer;
		ControlTimer<
			Timer7< Timer7InternalClockFeature, Timer7InterruptFeature >,
			Machine
			> outerCtrlTimer;
		GpioPinRef faultPin;

		Spi1<>::Parameters params;
		MPU6500<Spi1<>> mpu6500;
		volatile float gyroOffset;

		struct State {
			float x;
			float y;
			float phi;
			float rx;
			float ry;
			float rphi;
			float rvx;
			float rvy;
			float rw;
			float v;
			float w;
			State(int16_t _x,int16_t _y,int16_t _phi):
				x(_x),y(_y),phi(_phi),rx(_x),ry(_y),rphi(_phi),rvx(0),rvy(0),rw(0),v(0),w(0){}
			State():x(0),y(0),phi(0),rx(0),ry(0),rphi(0),rvx(0),rvy(0),rw(0),v(0),w(0){}
			void update(int16_t vl, int16_t vr){
				using namespace MyMath;
				using namespace MyMath::Machine;
				/*
				phi+=vl-vr;
				*/
				normalize(phi, PIInGyroValue);
				float phiInRadian=convertGyroValueToMachineRadian(phi);
				x+=-(float)(vr-vl)/2.0*sin(phiInRadian);
				y+=(float)(vr-vl)/2.0*cos(phiInRadian);
			}
			void calculateOutput(float &outputL,float &outputR){
				outputL=-v+w;
				outputR=v+w;
			}
		};
		State state;

		Trajectory::TargetSequence targetSequence;

		volatile bool activated;
		volatile bool blockTimer;
		volatile bool logEnabled;
		volatile bool wallCorrection;

		float adj;
		uint8_t faultCnt;

		uint16_t lastVelocity;

	public:
		Machine():
			//SIDE_NEUTRAL_LEFT(4800),
			//SIDE_NEUTRAL_RIGHT(4500),
			SIDE_NEUTRAL_LEFT(4800),
			SIDE_NEUTRAL_RIGHT(4500),
			//FRONT_THRESHOLD(600),
			FRONT_THRESHOLD(300),
			SIDE_THRESHOLD(2000),
			FRONT_ADJUST_THRESHOLD(1000),
			FRONT_NEUTRAL(7800),
			irLB(0),irLF(0),irRF(0),irRB(0),battery(0),
			timestamp(0),
			motorCtrlL(1.64862905257695f, 0.001f*114.470096465578f, 0.000475016889543637f/0.001f, 0.1531f, false, 0),
			motorCtrlR(1.64862905257695f, 0.001f*114.470096465578f, 0.000475016889543637f/0.001f, 0.1531f, false, 0),
			//angleCtrl(0.0266957032028978/MyMath::PI, 0.002f*0.0546410620260913/MyMath::PI, 3.24816955863286e-05f/0.002f, false, 10000),
			//angleCtrl(0.f, 0.f, 0.f, true, 0),
			angleCtrl(0.0266957032028978/8.f, 0.f, 1.624084779316430e-4f/0.002f, false, 5000),
			motorCtrlTimer(this,1000,4), // 1000Hz = 1ms cycle
			outerCtrlTimer(this,400,5),  //  400Hz = 2.5ms cycle
			faultPin(GpioC<DigitalInputFeature<GPIO_Speed_50MHz,Gpio::PUPD_UP,14>>()[14]),
			params([]{
					Spi1<>::Parameters params;
					params.spi_baudRatePrescaler = SPI_BaudRatePrescaler_16;
					params.spi_direction = SPI_Direction_2Lines_FullDuplex;
					return params;
					}()),
			mpu6500(params),
			gyroOffset(117.f),
			state(),
			targetSequence(0.0025f),
			activated(true),
			blockTimer(true),
			logEnabled(false),
			wallCorrection(true),
			adj(0.f),
			faultCnt(0),
			lastVelocity(0)
		{ }
		~Machine(){}

		void initialise(){
			debug<<"Testing MPU6050...\r\n";
			while(!mpu6500.test());
			debug<<"MPU6500 test passed.\r\n";

			MillisecondTimer::delay(1000);

			debug<<"Setting up MPU6500...\r\n";
			mpu6500.setup();
			debug<<"complete.\r\n";

			motorCtrlTimer.bind(&Machine::motorCtrlHandler);
			outerCtrlTimer.bind(&Machine::outerCtrlHandler);
			motorCtrlL.Encoder::setCounter(0);
			motorCtrlR.Encoder::setCounter(0);

			ir.initialise();

			Nvic::initialise();

		}

		void calibrateGyro(){
			// read yaw rate from gyroscope
			float gyrz=0.f;
			for(uint8_t i=0;i<250;i++){
				gyrz+=(float)mpu6500.readGyrZ();
				MillisecondTimer::delay(5);
			}
			gyroOffset=gyrz/250.f;
		}

		void activate(){
			activated=true;
		}
		void deactivate(){
			activated=false;
			motorCtrlL.stop();
			motorCtrlR.stop();
		}

		void block(){
			blockTimer=true;
		}
		void unblock(){
			blockTimer=false;
		}

		bool isActive() const{ return activated; }

		void resetControllers(){
			motorCtrlL.reset();
			motorCtrlR.reset();
			angleCtrl.reset();
		}

		uint16_t getLastVelocity() const { return lastVelocity; }
		Trajectory::Position getLastPosition() const { return targetSequence.getLastPosition(); }

		void motorCtrlHandler(TimerEventType te, uint8_t timerNumber){
			static float outputL=0;
			static float outputR=0;
			if(blockTimer) return;

#ifdef MAZEDEBUG
			motorCtrlL.stop();
			motorCtrlR.stop();
#endif

			motorCtrlL.Encoder::captureSpeed();
			motorCtrlR.Encoder::captureSpeed();
			// NOTE: should be called after Encoder::captureSpeed
			int16_t vl=motorCtrlL.Encoder::getSpeed();
			int16_t vr=motorCtrlR.Encoder::getSpeed();
			state.update(vl, vr);
			state.calculateOutput(outputL,outputR);

			//debug<<(int32_t)outputL<<","<<(int32_t)outputR<<endl;
#ifndef MAZEDEBUG
			// FAILSAFE
			if(abs(outputL) > 800 || abs(outputR) > 800){
				for(auto led : dleds) led.reset();
				dleds[0].set();
				deactivate();
			}
#endif

			if(activated){
#ifndef MAZEDEBUG
				motorCtrlL.update(outputL);
				motorCtrlR.update(outputR);
#endif

#ifndef MAZEDEBUG
				if(!faultPin.read()){
					//dleds[2].set();
					faultCnt++;
					if(faultCnt > 120) {
						for(auto led : dleds) led.reset();
						dleds[1].set();
						deactivate();
					}
				} else {
					//dleds[2].reset();
					faultCnt = 0;
				}
				//debugBufst << irLB <<","<< irLF <<","<< irRF <<","<< irRB <<","<< battery <<endl;
#endif
			}
			ir.clearDmaReady();
			ir.ledOn();
			for(volatile uint8_t i=0;i<120;i++);
			irLB = irLB * 0.8f + ir.getLB() * 0.2f;
			irLF = irLF * 0.8f + ir.getLF() * 0.2f;
			irRF = irRF * 0.8f + ir.getRF() * 0.2f;
			irRB = irRB * 0.8f + ir.getRB() * 0.2f;
			battery = ir.getBattery();
			ir.ledOff();
		}

		void outerCtrlHandler(TimerEventType te, uint8_t timerNumber){
			using namespace MyMath;
			using namespace MyMath::Machine;
			using namespace Trajectory;

			//static uint8_t noTargetCount = 0;
			static uint8_t lowBatteryCount = 0;
			static bool targetUpdated = false;

			if(blockTimer) return;

			// update current target
			if(!targetSequence.isEmpty()){
				//noTargetCount = 0;
				Position p,v,d;
				// increment timestamp while the current target is behind the machine
				//do {
					targetSequence.incrementTimestamp();
					p=convertRealPositionToMachinePosition(targetSequence.getCurrentPosition());
					v=convertRealPositionToMachinePosition(targetSequence.getCurrentVelocity());
					targetSequence.nextTarget();
					d = p - Position(state.x,state.y,state.phi);
				//} while(state.v > 20 &&  // machine is moving forward
				//		!targetSequence.isEmpty() &&  // target sequence is not empty
				//		fabs(normalized(d.angle,PIInGyroValue)) < 2.f * PIInGyroValue / 180.f &&  // the difference between current angle and the next target angle is less than 2 deg
				//		-d.x*sin(convertGyroValueToMachineRadian(state.phi))+d.y*cos(convertGyroValueToMachineRadian(state.phi))<0.f); // the target is behind the vehicle
				setReference(p.x, p.y, p.angle);
				setReferenceVelocity(v.x, v.y, v.angle);
				state.v = sqrt(v.x*v.x+v.y*v.y) * 0.4f;
				targetUpdated = true;
			} else {
				targetUpdated = false;
				// deactivate 200 frames (0.5s) after targets are no longer available
				/*
				if(activated) {
					noTargetCount++;
					if(noTargetCount>200) {
						deactivate();
						return;
					}
				}
				*/
			}

			if(activated){
				timestamp++;

#ifndef MAZEDEBUG
				// position feedback
				if(targetUpdated && fabs(state.v)>20){
					state.rphi += saturated((-(state.rx-state.x)*MyMath::cos(convertGyroValueToMachineRadian(state.phi))
							- (state.ry-state.y)*MyMath::sin(convertGyroValueToMachineRadian(state.phi))) * 0.4f, PIInGyroValue/4.f);
					state.v += saturated((-(state.rx-state.x)*MyMath::sin(convertGyroValueToMachineRadian(state.phi))
							+ (state.ry-state.y)*MyMath::cos(convertGyroValueToMachineRadian(state.phi))) * 0.05f, 50.f);
				}
#endif

				if(logEnabled && timestamp%10==0) {
					debugBufst
						<< (int32_t)(convertPulseToWheelDistance(state.rx)*10)<<","<<(int32_t)(convertPulseToWheelDistance(state.ry)*10)<<","<<(int32_t)((state.rphi*180.f/GyroValuePerMachineRotation)*10) << ","
						<< (int32_t)(convertPulseToWheelDistance(state.x)*10)<<","<<(int32_t)(convertPulseToWheelDistance(state.y)*10)<<","<<(int32_t)(convertGyroValueToMachineDegree(state.phi)*10) << ","
						<< (int32_t)state.v*10 << "," << (int32_t)state.w*10
						<< endl;
				}
#ifdef PRINTTARGET
				//if(timestamp%10==0) {
					debug
						<< (int32_t)(convertPulseToWheelDistance(state.rx)*10)<<","<<(int32_t)(convertPulseToWheelDistance(state.ry)*10)<<","<<(int32_t)((state.rphi*180.f/GyroValuePerMachineRotation)*10) << ","
						<< (int32_t)(convertPulseToWheelDistance(state.x)*10)<<","<<(int32_t)(convertPulseToWheelDistance(state.y)*10)<<","<<(int32_t)(convertGyroValueToMachineDegree(state.phi)*10) << ","
						<< (int32_t)state.v*10 << "," << (int32_t)state.w*10
						<< endl;
				//}
#endif

				// read yaw rate from gyroscope
				// TODO: fix this magic number to be calibrated
				float gyrz=(float)(mpu6500.readGyrZ()-gyroOffset)/16.f;
				state.phi += gyrz;

#ifndef MAZEDEBUG
				// FAILSAFE
				float dphi = state.rphi-state.phi;
				MyMath::normalize(dphi, PIInGyroValue);
				if(MyMath::fabs(dphi) >= PIInGyroValue/4.f) {
					for(auto led : dleds) led.reset();
					dleds[2].set();
					deactivate();
				}
#endif

#ifdef MAZEDEBUG
				state.phi = state.rphi;
#endif

				angleCtrl.update(state.rphi, state.phi);
				state.w = angleCtrl.getOutput();

				// display wall detection
				if(activated) {
					dleds[0].reset(); dleds[1].reset(); dleds[2].reset();
					if(irLF > SIDE_THRESHOLD) {
						dleds[2].set();
					}
					if(irRF > SIDE_THRESHOLD) {
						dleds[0].set();
					}
					if(irRB > FRONT_THRESHOLD && irLB > FRONT_THRESHOLD) {
						dleds[1].set();
					}
				}

				// wall adjustment
				if(wallCorrection && fabs(state.v)>20){
					//float rphiInRadian=MyMath::Machine::convertGyroValueToMachineRadian(state.rphi);
					if(irLF>SIDE_THRESHOLD&&irRF>SIDE_THRESHOLD){
						// not close to the front wall and
						// there are walls both on left and right
						adj=adj*0.9f+(float)(irLF-irRF)/2000.f;
						state.w-=adj;
					//} else if(irLF>SIDE_NEUTRAL_LEFT+500.f && irLB<FRONT_ADJUST_THRESHOLD && irRB<FRONT_ADJUST_THRESHOLD){
					} else if(irRF<SIDE_THRESHOLD && irLF>SIDE_THRESHOLD && irLB<FRONT_ADJUST_THRESHOLD && irRB<FRONT_ADJUST_THRESHOLD){
						// not close to the front wall and
						// there is a wall only on the left
						adj=adj*0.9f+(float)(irLF-SIDE_NEUTRAL_LEFT)/4000.f;
						state.w-=adj;
					//} else if(irRF>SIDE_NEUTRAL_RIGHT+500.f && irLB<FRONT_ADJUST_THRESHOLD && irRB<FRONT_ADJUST_THRESHOLD){
					} else if(irLF<SIDE_THRESHOLD && irRF>SIDE_THRESHOLD && irLB<FRONT_ADJUST_THRESHOLD && irRB<FRONT_ADJUST_THRESHOLD){
						// not close to the front wall and
						// there is a wall only on the right
						adj=adj*0.9f+(float)(-irRF+SIDE_NEUTRAL_RIGHT)/4000.f;
						state.w-=adj;
					}
				} else {
					adj=adj*0.9f;
				}

				if(battery < 2900 * 2) {
					lowBatteryCount++;
					if(lowBatteryCount > 200) {
						for(auto led : dleds) led.reset();
						dleds[3].set();
						deactivate();
					}
				} else {
					lowBatteryCount = 0;
				}
				//debug << irLB <<","<< irLF <<","<< irRF <<","<< irRB <<","<< battery <<endl;
			}
		}

		uint16_t getEncoderCounter(){
			return motorCtrlR.Encoder::getCounter();
		}
		void setEncoderCounter(uint16_t val){
			motorCtrlR.Encoder::setCounter(val);
		}

		bool isSetWall(uint8_t dir){
			uint16_t irRBA=irRB, irLBA=irLB;
			if((dir&0xf)==0x1){
				return (irLBA>FRONT_THRESHOLD && irRBA>FRONT_THRESHOLD);
			} else if((dir&0xf)==0x2){
				return (irRF>SIDE_THRESHOLD);
			} else if((dir&0xf)==0x8){
				return (irLF>SIDE_THRESHOLD);
			}
			return false;
		}

		uint16_t getIrLF() const { return irLF; }
		uint16_t getIrLB() const { return irLB; }
		uint16_t getIrRF() const { return irRF; }
		uint16_t getIrRB() const { return irRB; }
		uint16_t getBattery() const { return battery; }

		void setThresholds(uint16_t f, uint16_t s, uint16_t fa, uint16_t fn){
			FRONT_THRESHOLD=f;
			SIDE_THRESHOLD=s;
			FRONT_ADJUST_THRESHOLD=fa;
			FRONT_NEUTRAL=fn;
		}

		void setNeutralSideSensorValue(){
			SIDE_NEUTRAL_LEFT=irLF;
			SIDE_NEUTRAL_RIGHT=irRF;
		}

		void setActiveThresholds(){
			//setThresholds(300,2000,1000,7800); // full-size half
			//setThresholds(1100,2000,1000,7800); // full-size competition
			setThresholds(300,2000,1000,7800); // half-home
			//setThresholds(1400,2000,3600,6000); // full-size univ
			//setThresholds(1100,1800,3000,6800); // half-size
		}

		void setReference(float rx,float ry,float rphi) volatile{
			using namespace MyMath;
			using namespace MyMath::Machine;
			state.rx=rx;
			state.ry=ry;
			normalize(rphi, PIInGyroValue);
			state.rphi=rphi;
		}

		void setReferenceVelocity(float rvx,float rvy,float rw) volatile{
			state.rvx=rvx;
			state.rvy=rvy;
			state.rw=rw;
		}
		void setState(float x,float y,float phi) volatile{
			state.x=MyMath::Machine::convertWheelDistanceToPulse(x);
			state.y=MyMath::Machine::convertWheelDistanceToPulse(y);
			state.phi=MyMath::Machine::convertMachineRadianToGyroValue(phi);
			state.rx=state.x;
			state.ry=state.y;
			state.rphi=state.phi;
			state.v=0;
			state.w=0;
		}

		void pushTargetDiff(const Trajectory::Position& diff, Trajectory::MotionFunctor *mf, Trajectory::Parameters p){
			targetSequence.pushDifference(diff, mf, p);
			lastVelocity=p.vf;
		}

		void pushTarget(const Trajectory::Position& pos, Trajectory::MotionFunctor* mf, Trajectory::Parameters p){
			targetSequence.push(pos, mf, p);
			lastVelocity=p.vf;
		}

		bool isTargetSequenceEmpty() const{
			return targetSequence.isEmpty();
		}

		void resetTargetSequence(){
			targetSequence.resetSequence();
		}

		void setWallCorrection(bool w){
			wallCorrection=w;
		}

		void enableLog() { logEnabled = true; }
		void disableLog() { logEnabled = false; }
};
