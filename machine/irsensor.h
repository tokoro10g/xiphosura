#pragma once

#include "config/stm32plus.h"
#include "config/adc.h"

using namespace stm32plus;

class IRSensor {
public:
	IRSensor():dmaReady(false){
	}
	~IRSensor(){}

	void initialise(){
		dma.DmaInterruptEventSender.insertSubscriber(
				DmaInterruptEventSourceSlot::bind(this, &IRSensor::onAdcComplete)
				);
		dma.setNvicPriorities(3);
		dma.enableInterrupts(Adc1DmaChannelInterruptFeature::COMPLETE);

		dma.beginRead(buffer, 10);
		adc.startRegularConversion();
	}

	void copyBuffer(uint16_t* dest) {
		memcpy(dest, (const void*)buffer, 16);
	}

	void ledOn() {
		irledPort[8].set();
	}
	void ledOff() {
		irledPort[8].reset();
	}
	bool ledToggle() {
		bool newState = !irledPort[8].read();
		irledPort[8].setState(newState);
		return newState;
	}
	bool isDmaReady() const{
		return dmaReady;
	}
	void clearDmaReady(){
		dmaReady=false;
	}
	volatile uint16_t getLB() const{
		return buffer[0]+buffer[4];
	}
	volatile uint16_t getLF() const{
		return buffer[2]+buffer[6];
	}
	volatile uint16_t getRF() const{
		return buffer[1]+buffer[5];
	}
	volatile uint16_t getRB() const{
		return buffer[3]+buffer[7];
	}
	uint16_t getBattery() const{
		return buffer[8]+buffer[9];
	}

private:
	volatile uint16_t buffer[10];
	Adc1DmaChannel<AdcMultiDmaMode1Feature<Adc1PeripheralTraits>, Adc1DmaChannelInterruptFeature> dma;
	Adc1<
		AdcClockPrescalerFeature<2>,
		AdcResolutionFeature<12>,
		Adc1Cycle28RegularChannelFeature<10,11,10,11,9>,
		AdcScanModeFeature<>,
		AdcContinuousModeFeature,
		AdcDualRegularSimultaneousDmaMode1Feature<Adc2<
			AdcClockPrescalerFeature<2>,
			AdcResolutionFeature<12>,
			Adc2Cycle28RegularChannelFeature<12,13,12,13,9>,
			AdcScanModeFeature<>,
			AdcContinuousModeFeature
			>, 5>
		> adc;
	volatile bool dmaReady;

	GpioB<DefaultDigitalOutputFeature<8>> irledPort;

	void onAdcComplete(DmaEventType det){
		if(det==DmaEventType::EVENT_COMPLETE){
			dmaReady=true;
		}
	}
};
