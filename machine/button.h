/*
 * This file is a part of the open source stm32plus library.
 * Copyright (c) 2011,2012,2013,2014,2015 Andy Brown <www.andybrown.me.uk>
 * Please see website for licensing terms.
 */

#include "config/stm32plus.h"
#include "config/button.h"

using namespace stm32plus;

class EdgeButton {

	private:

		/// The pin
		GpioPinRef _pin;

	public:

		/**
		 * Possible button states
		 */

		enum ButtonState {
			High,
			Low,
			RisingEdge,
			FallingEdge
		} _internalState;

		/**
		 * Constructor
		 *
		 * @param[in] pin The pin to use.
		 */

		EdgeButton(const GpioPinRef& pin): _pin(pin) {
			bool state = _pin.read();
			_internalState = state ? High : Low;
		}

		/**
		 * Update the current state
		 * Compare current state with last state to detect the rising and falling edges.
		 */

		void updateState() {
			bool state = _pin.read();
			if(_internalState == FallingEdge || _internalState == RisingEdge) {
				_internalState = state ? High : Low;
			} else if(_internalState == High && !state) {
				_internalState = FallingEdge;
			} else if(_internalState == Low && state) {
				_internalState = RisingEdge;
			}
		}

		/**
		 * Get the current state
		 * @return The current state
		 */

		ButtonState getState() {
			return _internalState;
		}

};
