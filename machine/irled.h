#pragma once

#include "config/stm32plus.h"
#include "config/gpio.h"

#include <vector>
#include <iterator>

using namespace stm32plus;

template<typename... Pins>
class IRLED {
public:
	IRLED(const Pins&... _pins){
		addPins(_pins...);
	}
	~IRLED(){}
	template<typename Pin, typename... Rest>
	void addPins(const Pin& _pin, const Rest&... _rest){
		pins.push_back(_pin);
		return addPins(_rest...);
	}
	template<typename Pin>
	void addPins(const Pin& _pin){
		pins.push_back(_pin);
	}

	GpioPinRef operator[](int index){
		return pins[index];
	}

	void turnOn() const{
		for(std::vector<GpioPinRef>::const_iterator it=pins.begin(),end=pins.end();it!=end;it++){
			(*it).set();
		}
	}
	void turnOff() const{
		for(std::vector<GpioPinRef>::const_iterator it=pins.begin(),end=pins.end();it!=end;it++){
			(*it).reset();
		}
	}
private:
	std::vector<GpioPinRef> pins;
};
