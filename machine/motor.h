#pragma once

#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timer.h"

namespace stm32plus{
	template<class ChannelA, class ChannelB>
	class Motor : public Timer2<
				   Timer2InternalClockFeature,
				   ChannelA,
				   ChannelB,
				   Timer2GpioFeature<
							TIMER_REMAP_NONE,
							TIM2_CH1_OUT,
							TIM2_CH2_OUT,
							TIM2_CH3_OUT,
							TIM2_CH4_OUT > > {
		public:
			enum{
				MAX_COMPARE=200
			};
			Motor():Timer2<
				   Timer2InternalClockFeature,
				   ChannelA,
				   ChannelB,
				   Timer2GpioFeature<
							TIMER_REMAP_NONE,
							TIM2_CH1_OUT,
							TIM2_CH2_OUT,
							TIM2_CH3_OUT,
							TIM2_CH4_OUT > >(){
				this->setTimeBaseByFrequency(10000000, MAX_COMPARE);
				ChannelA::initCompareForPwmOutput();
				ChannelB::initCompareForPwmOutput();
				this->enablePeripheral();
			}
			inline void setOutput(int16_t value){
				if(value>MAX_COMPARE) value=MAX_COMPARE;
				if(value<-MAX_COMPARE) value=-MAX_COMPARE;
				if(value>0){
					ChannelA::setCompare(MAX_COMPARE);
					ChannelB::setCompare(MAX_COMPARE-value);
				} else {
					ChannelA::setCompare(MAX_COMPARE+value);
					ChannelB::setCompare(MAX_COMPARE);
				}
			}
			inline void stop(){
				ChannelA::setCompare(0);
				ChannelB::setCompare(0);
			}
	};

	class Motors : public Timer2<
				   Timer2InternalClockFeature,
				   TimerChannel1Feature<>,
				   TimerChannel2Feature<>,
				   TimerChannel3Feature<>,
				   TimerChannel4Feature<>,
				   Timer2GpioFeature<
							TIMER_REMAP_NONE,
							TIM2_CH1_OUT,
							TIM2_CH2_OUT,
							TIM2_CH3_OUT,
							TIM2_CH4_OUT > > {
		public:
			enum{
				MAX_COMPARE=200
			};
			Motors():Timer2(){
				this->setTimeBaseByFrequency(5000000, MAX_COMPARE);
				TimerChannel1Feature<>::initCompareForPwmOutput();
				TimerChannel2Feature<>::initCompareForPwmOutput();
				TimerChannel3Feature<>::initCompareForPwmOutput();
				TimerChannel4Feature<>::initCompareForPwmOutput();
				this->enablePeripheral();
			}
			inline void setOutput(int16_t left,int16_t right){
				if(left>MAX_COMPARE) left=MAX_COMPARE;
				if(left<-MAX_COMPARE) left=-MAX_COMPARE;
				if(right>MAX_COMPARE) right=MAX_COMPARE;
				if(right<-MAX_COMPARE) right=-MAX_COMPARE;
				if(left>0){
					TimerChannel3Feature<>::setCompare(MAX_COMPARE-left);
					TimerChannel4Feature<>::setCompare(MAX_COMPARE);
				} else {
					TimerChannel3Feature<>::setCompare(MAX_COMPARE);
					TimerChannel4Feature<>::setCompare(MAX_COMPARE+left);
				}
				if(right>0){
					TimerChannel1Feature<>::setCompare(MAX_COMPARE);
					TimerChannel2Feature<>::setCompare(MAX_COMPARE-right);
				} else {
					TimerChannel1Feature<>::setCompare(MAX_COMPARE+right);
					TimerChannel2Feature<>::setCompare(MAX_COMPARE);
				}
			}
			inline void stopAll(){
				setOutput(0,0);
			}
		private:
	};
}
